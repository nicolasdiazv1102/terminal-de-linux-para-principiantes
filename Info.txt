Terminal de Linux Para Principiantes

Link Youtube: https://www.youtube.com/watch?v=bIHAF0WVj7I
ink Gitlab: https://gitlab.com/nicolasdiazv1102/terminal-de-linux-para-principiantes.git

23 mar 2022  

En este video se hablará de conceptos básicos de la terminal de GNU/Linux 
y comandos básicos de la misma, tales como moverse entre directorios, 
crear, copiar, mover y eliminar archivos y carpetas, ver manuales de comandos 
y comprimir archivos, entre otros.
Todo esto con el objetivo principal de ayudar a las personas a perderle el terror 
de enfrentarse a una terminal y obtengan los conocimientos necesarios para 
entender como funciona la misma.

Licencia Atribución de Creative Commons (reutilización permitida)
